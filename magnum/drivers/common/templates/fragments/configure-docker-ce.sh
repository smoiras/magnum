#!/bin/sh

. /etc/sysconfig/heat-params

if [ "$DOCKER_CE_VERSION" = "" ]; then
    exit 0
fi

setenforce 0
systemctl stop docker
systemctl disable docker
atomic install --system --system-package no --name docker gitlab-registry.cern.ch/cloud/docker-ce-centos:$DOCKER_CE_VERSION
systemctl start docker
