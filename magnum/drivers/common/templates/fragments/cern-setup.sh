#!/bin/bash

set +x
. /etc/sysconfig/heat-params
set -x
ssh_cmd="ssh -F /srv/magnum/.ssh/config root@localhost"
cd /etc/pki/ca-trust/source/anchors/

BASE_URL=https://cafiles.cern.ch/cafiles/certificates/
CERTS=( "CERN Root Certification Authority 2" "CERN Grid Certification Authority" "CERN Certification Authority" )

for CERT in "${CERTS[@]}"
do
  set +x
  URL="$BASE_URL$CERT.crt"
  curl "$( echo "$URL" | sed 's/ /%20/g' )" -o "$CERT.crt"
  FIRST_LINE=$(head -1 "$CERT.crt")
  if [[ ! $FIRST_LINE == *"BEGIN CERTIFICATE"* ]]; then
      openssl x509 -inform DER -in "$CERT.crt" -out "$CERT.pem"
      rm "$CERT.crt"
  else
      mv "$CERT.crt" "$CERT.pem"
  fi
done
set -x

rc=0
update-ca-trust || rc=$?
if [ ! $rc -eq "0" ]; then
    echo "WARNING update-ca-trust exited with return code: $rc"
fi

cern_enabled=$(echo "${CERN_ENABLED}" | tr '[:upper:]' '[:lower:]')
cern_chart_enabled=$(echo "${CERN_CHART_ENABLED}" | tr '[:upper:]' '[:lower:]')

if [ "${cern_enabled}" != "true" ] || [ "${cern_chart_enabled}" = "true" ]; then
    echo "cern_enabled set to ${cern_enabled} (!true) or cern_chart_enabled set to ${cern_chart_enabled}, skipping cern certificate setup with atomic/podman"
else
    if [ "$(echo $USE_PODMAN | tr '[:upper:]' '[:lower:]')" == "true" ]; then
        cat > /etc/systemd/system/cern-keytab.service <<EOF
[Unit]
Description=Run cern-keytab
After=network-online.target
Wants=network-online.target

[Service]
ExecStartPre=mkdir -p /srv/magnum
ExecStartPre=-/bin/podman kill cern-keytab
ExecStartPre=-/bin/podman rm cern-keytab
ExecStartPre=-/bin/podman pull gitlab-registry.cern.ch/cloud/cern-keytab:${CERN_TAG:-latest}
ExecStart=/bin/podman run \\
    --name cern-keytab \\
    --net=host \\
    --privileged \\
    --volume /etc/:/etc-host:rw \\
    --volume /tmp:/tmp \\
    --volume /dev:/dev \\
    gitlab-registry.cern.ch/cloud/cern-keytab:${CERN_TAG:-latest} \\
    /doit.sh
ExecStop=/bin/podman stop cern-keytab
TimeoutStartSec=10min

[Install]
WantedBy=multi-user.target
EOF
        $ssh_cmd systemctl enable cern-keytab
    else
        $ssh_cmd atomic install --name cern-keytab --system gitlab-registry.cern.ch/cloud/cern-keytab:${CERN_TAG:-latest}
    fi
    $ssh_cmd systemctl start cern-keytab

    while [[ ! -f /etc/krb5.keytab ]]
    do
      echo "waiting for cern-keytab generation... sleeping 10 seconds"
      sleep 10
    done

    if [ "$(echo $USE_PODMAN | tr '[:upper:]' '[:lower:]')" == "true" ]; then
        cat > /etc/systemd/system/cern-hostcert.service <<EOF
[Unit]
Description=Run cern-hostcert
After=network-online.target
Wants=network-online.target

[Service]
ExecStartPre=mkdir -p /srv/magnum
ExecStartPre=-/bin/podman kill cern-hostcert
ExecStartPre=-/bin/podman rm cern-hostcert
ExecStartPre=-/bin/podman pull gitlab-registry.cern.ch/cloud/cern-hostcert:${CERN_TAG:-latest}
ExecStart=/bin/podman run \\
    --name cern-hostcert \\
    --net=host \\
    --privileged \\
    --volume /etc/:/etc-host:rw \\
    --volume /tmp:/tmp \\
    --volume /dev:/dev \\
    gitlab-registry.cern.ch/cloud/cern-hostcert:${CERN_TAG:-latest} \\
    /doit.sh
ExecStop=/bin/podman stop cern-hostcert
TimeoutStartSec=10min

[Install]
WantedBy=multi-user.target
EOF
        $ssh_cmd systemctl enable cern-hostcert
    else
        $ssh_cmd atomic install --name cern-hostcert --system gitlab-registry.cern.ch/cloud/cern-hostcert:${CERN_TAG:-latest}
    fi
    $ssh_cmd systemctl start cern-hostcert
fi
