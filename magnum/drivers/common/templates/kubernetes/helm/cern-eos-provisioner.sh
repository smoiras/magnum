#!/bin/bash

. /etc/sysconfig/heat-params

set -ex

step="cern-eos-provisioner"
printf "Starting to run ${step}\n"

# check the actual version of kubernetes deployed
kube_version=$(curl -k -s https://localhost:6443/version?timeout=32s | sed -rn 's/.*minor": "([[:digit:]]+).*/\1/p')

### Configuration
###############################################################################
CHART_NAME="eosxd"

# Check if prometheus monitoring is enabled and if user specified a METRICS_PRODUCER
if [ "$(echo ${CERN_CHART_ENABLED} | tr '[:upper:]' '[:lower:]')" != "true" ] && [ "$(echo ${EOS_ENABLED} | tr '[:upper:]' '[:lower:]')" = "true" ]; then
    HELM_MODULE_CONFIG_FILE="/srv/magnum/kubernetes/helm/${CHART_NAME}.yaml"
    [ -f ${HELM_MODULE_CONFIG_FILE} ] || {
        echo "Writing File: ${HELM_MODULE_CONFIG_FILE}"
        mkdir -p $(dirname ${HELM_MODULE_CONFIG_FILE})
        cat << EOF > ${HELM_MODULE_CONFIG_FILE}
---
kind: ConfigMap
apiVersion: v1
metadata:
  name: ${CHART_NAME}-config
  namespace: magnum-tiller
  labels:
    app: helm
data:
  install-${CHART_NAME}.sh: |
    #!/bin/bash
    set -ex
    mkdir -p \${HELM_HOME}
    cp /etc/helm/* \${HELM_HOME}

    # HACK - Force wait because of bug https://github.com/helm/helm/issues/5170
    until helm init --client-only --wait
    do
        sleep 5s
    done
    helm repo add cern https://registry.cern.ch/chartrepo/cern
    helm repo update

    if [[ \$(helm history ${CHART_NAME} | grep ${CHART_NAME}) ]]; then
        echo "${CHART_NAME} already installed on server. Continue..."
        exit 0
    else
        helm install cern/${CHART_NAME} --namespace kube-system --name ${CHART_NAME} --version ${EOS_CHART_TAG} --values /opt/magnum/install-${CHART_NAME}-values.yaml
    fi

  install-${CHART_NAME}-values.yaml:  |
    mounts:
      ams:
      atlas:
      cms:
      experiment:
      lhcb:
      project:
        # project-i00: "a e j g v k q y"
        # project-i01: "l h b p s f w n o"
        # project-i02: "d c i r m t u x z"
      theory:
      user:
        # home-i00: "l n t z"
        # home-i01: "a g j k w"
        # home-i02: "h o r s y"
        # home-i03: "b e m v x"
        # home-i04: "c f i p q"
      workspace:

---

apiVersion: batch/v1
kind: Job
metadata:
  name: install-${CHART_NAME}-job
  namespace: magnum-tiller
spec:
  backoffLimit: 5
  template:
    spec:
      serviceAccountName: tiller
      containers:
      - name: config-helm
        image: ${CONTAINER_INFRA_PREFIX:-docker.io/openstackmagnum/}helm-client:dev
        command:
        - bash
        args:
        - /opt/magnum/install-${CHART_NAME}.sh
        env:
        - name: HELM_HOME
          value: /helm_home
        - name: TILLER_NAMESPACE
          value: magnum-tiller
        - name: HELM_TLS_ENABLE
          value: "true"
        volumeMounts:
        - name: install-${CHART_NAME}-config
          mountPath: /opt/magnum/
        - mountPath: /etc/helm
          name: helm-client-certs
      restartPolicy: Never
      volumes:
      - name: install-${CHART_NAME}-config
        configMap:
          name: ${CHART_NAME}-config
      - name: helm-client-certs
        secret:
          secretName: helm-client-secret
EOF
    }

fi

printf "Finished running ${step}\n"
