#!/bin/bash

. /etc/sysconfig/heat-params

set -ex

step="cern-chart"
printf "Starting to run ${step}\n"

### Configure installation dependencies
###############################################################################
if [ "$(echo ${MONITORING_ENABLED} | tr '[:upper:]' '[:lower:]')" = "true" ] && \
   [ "$(echo ${METRICS_PRODUCER})" != "" ]; then
    CERN_CENTRAL_MONITORING="true"
else
    CERN_CENTRAL_MONITORING="false"
fi

if [ "$(echo ${LOGGING_INSTALLER} | tr '[:upper:]' '[:lower:]')" = "helm" ] && \
   [ ! -z "${LOGGING_PRODUCER}" ]; then
    CERN_CENTRAL_LOGGING="true"
else
    CERN_CENTRAL_LOGGING="false"
fi

NVIDIA_GPU_VALUES=""
if [ "$(echo ${NVIDIA_GPU_TAG})" != "" ]; then
    NVIDIA_GPU_VALUES="
      plugin:
        image:
          tag: ${NVIDIA_GPU_TAG}
      installer:
        image:
          tag: ${NVIDIA_GPU_TAG}
"
fi

### Configuration
###############################################################################
CHART_NAME="cern-magnum"

if [ "$(echo ${CERN_CHART_ENABLED} | tr '[:upper:]' '[:lower:]')" = "true" ]; then
    HELM_MODULE_CONFIG_FILE="/srv/magnum/kubernetes/helm/${CHART_NAME}.yaml"
    [ -f ${HELM_MODULE_CONFIG_FILE} ] || {
        echo "Writing File: ${HELM_MODULE_CONFIG_FILE}"
        mkdir -p $(dirname ${HELM_MODULE_CONFIG_FILE})
        cat << EOF > ${HELM_MODULE_CONFIG_FILE}
---
kind: ConfigMap
apiVersion: v1
metadata:
  name: ${CHART_NAME}-config
  namespace: magnum-tiller
  labels:
    app: helm
data:
  install-${CHART_NAME}.sh: |
    #!/bin/bash
    set -ex
    mkdir -p \${HELM_HOME}
    cp /etc/helm/* \${HELM_HOME}

    # HACK - Force wait because of bug https://github.com/helm/helm/issues/5170
    until helm init --client-only --wait
    do
        sleep 5s
    done
    helm repo add releases https://registry.cern.ch/chartrepo/releases
    helm repo update

    if [[ \$(helm history ${CHART_NAME} | grep ${CHART_NAME}) ]]; then
        echo "${CHART_NAME} already installed on server. Continue..."
        exit 0
    else
        helm install releases/${CHART_NAME} --wait --namespace kube-system --name ${CHART_NAME} --version ${CERN_CHART_VERSION} --values /opt/magnum/install-${CHART_NAME}-values.yaml
    fi

  install-${CHART_NAME}-values.yaml:  |
    eosxd:
      enabled: ${EOS_ENABLED}
    nvidia-gpu:
      enabled: ${NVIDIA_GPU_ENABLED}
${NVIDIA_GPU_VALUES}
    fluentd:
      enabled: ${CERN_CENTRAL_LOGGING}
      output:
        producer: ${LOGGING_PRODUCER}
        endpoint: ${LOGGING_HTTP_DESTINATION}
        includeInternal: ${LOGGING_INCLUDE_INTERNAL}
      containerRuntime: ${CONTAINER_RUNTIME}
    landb-sync:
      enabled: ${LANDB_SYNC_ENABLED}
    prometheus-cern:
      enabled: ${CERN_CENTRAL_MONITORING}
    openstack-cinder-csi:
      enabled: ${CINDER_CSI_ENABLED}
    base:
      enabled: ${CERN_ENABLED}

---

apiVersion: batch/v1
kind: Job
metadata:
  name: install-${CHART_NAME}-job
  namespace: magnum-tiller
spec:
  backoffLimit: 5
  template:
    spec:
      serviceAccountName: tiller
      containers:
      - name: config-helm
        image: ${CONTAINER_INFRA_PREFIX:-docker.io/openstackmagnum/}helm-client:dev
        command:
        - bash
        args:
        - /opt/magnum/install-${CHART_NAME}.sh
        env:
        - name: HELM_HOME
          value: /helm_home
        - name: TILLER_NAMESPACE
          value: magnum-tiller
        - name: HELM_TLS_ENABLE
          value: "true"
        volumeMounts:
        - name: install-${CHART_NAME}-config
          mountPath: /opt/magnum/
        - mountPath: /etc/helm
          name: helm-client-certs
      restartPolicy: Never
      volumes:
      - name: install-${CHART_NAME}-config
        configMap:
          name: ${CHART_NAME}-config
      - name: helm-client-certs
        secret:
          secretName: helm-client-secret
EOF
    }

fi

printf "Finished running ${step}\n"
