#!/bin/bash

. /etc/sysconfig/heat-params

set -ex

step="cern-prometheus-rules"
printf "Starting to run ${step}\n"

### Configuration
###############################################################################
CHART_NAME="prometheus-cern"
CHART_VERSION=${METRICS_PRODUCER_VERSION}

# Check if prometheus monitoring is enabled and if user specified a METRICS_PRODUCER
if [ "$(echo ${CERN_CHART_ENABLED} | tr '[:upper:]' '[:lower:]')" != "true" ] && \
   [ "$(echo ${MONITORING_ENABLED} | tr '[:upper:]' '[:lower:]')" = "true" ] && \
   [ "$(echo ${METRICS_PRODUCER})" != "" ]; then
    HELM_MODULE_CONFIG_FILE="/srv/magnum/kubernetes/helm/${CHART_NAME}.yaml"
    [ -f ${HELM_MODULE_CONFIG_FILE} ] || {
        echo "Writing File: ${HELM_MODULE_CONFIG_FILE}"
        mkdir -p $(dirname ${HELM_MODULE_CONFIG_FILE})
        cat << EOF > ${HELM_MODULE_CONFIG_FILE}
---
kind: ConfigMap
apiVersion: v1
metadata:
  name: ${CHART_NAME}-config
  namespace: magnum-tiller
  labels:
    app: helm
data:
  install-${CHART_NAME}.sh: |
    #!/bin/bash
    set -ex
    mkdir -p \${HELM_HOME}
    cp /etc/helm/* \${HELM_HOME}

    # HACK - Force wait because of bug https://github.com/helm/helm/issues/5170
    until helm init --client-only --wait
    do
        sleep 5s
    done
    helm repo add cern https://registry.cern.ch/chartrepo/cern
    helm repo update

    if [[ \$(helm history ${CHART_NAME} | grep ${CHART_NAME}) ]]; then
        echo "${CHART_NAME} already installed on server. Continue..."
        exit 0
    else
        # prometheus-cern chart needs the Prometheus-Operator CRDs to create
        # the PrometheusRules K8s manifests. Check if CRD is registered:
        while : ; do
            # TODO: Set namespace to monitoring. This is needed as the Kubernetes default priorityClass can only be used in NS kube-system
            # TODO: Find a better way to check if the required CRDs are available
            helm install cern/${CHART_NAME} --namespace kube-system --name ${CHART_NAME} --version ${CHART_VERSION}
            [[ $? == 1 ]] || break
            helm delete --purge ${CHART_NAME}
            sleep 5
        done
    fi

---

apiVersion: batch/v1
kind: Job
metadata:
  name: install-${CHART_NAME}-job
  namespace: magnum-tiller
spec:
  backoffLimit: 5
  template:
    spec:
      serviceAccountName: tiller
      containers:
      - name: config-helm
        image: ${CONTAINER_INFRA_PREFIX:-docker.io/openstackmagnum/}helm-client:dev
        command:
        - bash
        args:
        - /opt/magnum/install-${CHART_NAME}.sh
        env:
        - name: HELM_HOME
          value: /helm_home
        - name: TILLER_NAMESPACE
          value: magnum-tiller
        - name: HELM_TLS_ENABLE
          value: "true"
        volumeMounts:
        - name: install-${CHART_NAME}-config
          mountPath: /opt/magnum/
        - mountPath: /etc/helm
          name: helm-client-certs
      restartPolicy: Never
      volumes:
      - name: install-${CHART_NAME}-config
        configMap:
          name: ${CHART_NAME}-config
      - name: helm-client-certs
        secret:
          secretName: helm-client-secret
EOF
    }

fi

printf "Finished running ${step}\n"
