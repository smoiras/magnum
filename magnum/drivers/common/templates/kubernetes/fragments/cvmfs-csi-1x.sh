    EXTERNAL_PROVISIONER_IMAGE="${CONTAINER_INFRA_PREFIX:-quay.io/k8scsi/}csi-provisioner:${KUBE_CSI_VERSION}"
    EXTERNAL_ATTACHER_IMAGE="${CONTAINER_INFRA_PREFIX:-quay.io/k8scsi/}csi-attacher:${KUBE_CSI_VERSION}"
    DRIVER_REGISTRAR_IMAGE="${CONTAINER_INFRA_PREFIX:-quay.io/k8scsi/}driver-registrar:${KUBE_CSI_VERSION}"
    CVMFS_PLUGIN_IMAGE="${CONTAINER_INFRA_PREFIX:-dockerhub/cvmfs/}cvmfsplugin:${CVMFS_CSI_VERSION}"
    CVMFS_CSI_DEPLOY=/srv/magnum/kubernetes/cvmfs-csi.yaml

    [ -f ${CVMFS_CSI_DEPLOY} ] || {
        echo "Writing File: $CVMFS_CSI_DEPLOY"
        mkdir -p $(dirname ${CVMFS_CSI_DEPLOY})
        cat << EOF > ${CVMFS_CSI_DEPLOY}
apiVersion: v1
kind: ServiceAccount
metadata:
  name: cvmfs-csi-nodeplugin
  namespace: kube-system
---
apiVersion: policy/v1beta1
kind: PodSecurityPolicy
metadata:
  name: cvmfs.privileged
  annotations:
    kubernetes.io/description: 'privileged allows full unrestricted access to
      pod features, as if the PodSecurityPolicy controller was not enabled.'
    seccomp.security.alpha.kubernetes.io/allowedProfileNames: '*'
  labels:
    kubernetes.io/cluster-service: "true"
    addonmanager.kubernetes.io/mode: Reconcile
spec:
  privileged: true
  allowPrivilegeEscalation: true
  allowedCapabilities:
  - '*'
  volumes:
  - '*'
  hostNetwork: true
  hostPorts:
  - min: 0
    max: 65535
  hostIPC: true
  hostPID: true
  runAsUser:
    rule: 'RunAsAny'
  seLinux:
    rule: 'RunAsAny'
  supplementalGroups:
    rule: 'RunAsAny'
  fsGroup:
    rule: 'RunAsAny'
  readOnlyRootFilesystem: false
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRole
metadata:
  name: cvmfs:podsecuritypolicy:privileged
  labels:
    kubernetes.io/cluster-service: "true"
    addonmanager.kubernetes.io/mode: Reconcile
rules:
- apiGroups:
  - policy
  resourceNames:
  - cvmfs.privileged
  resources:
  - podsecuritypolicies
  verbs:
  - use
---
apiVersion: rbac.authorization.k8s.io/v1
kind: RoleBinding
metadata:
  name: cvmfs:podsecuritypolicy:cvmfs-csi-nodeplugin
  namespace: kube-system
  labels:
    addonmanager.kubernetes.io/mode: Reconcile
    kubernetes.io/cluster-service: "true"
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cvmfs:podsecuritypolicy:privileged
subjects:
- kind: ServiceAccount
  name: cvmfs-csi-nodeplugin
  namespace: kube-system
---
apiVersion: rbac.authorization.k8s.io/v1
kind: RoleBinding
metadata:
  name: cvmfs:podsecuritypolicy:cvmfs-csi-provisioner
  namespace: kube-system
  labels:
    addonmanager.kubernetes.io/mode: Reconcile
    kubernetes.io/cluster-service: "true"
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: magnum:podsecuritypolicy:privileged
subjects:
- kind: ServiceAccount
  name: cvmfs-csi-provisioner
  namespace: kube-system

---
kind: ClusterRole
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  name: cvmfs-csi-nodeplugin
aggregationRule:
  clusterRoleSelectors:
    - matchLabels:
        rbac.cvmfs.csi.cern.ch/aggregate-to-cvmfs-csi-nodeplugin: "true"
rules: []
---
kind: ClusterRole
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  name: cvmfs-csi-nodeplugin-rules
  labels:
    rbac.cvmfs.csi.cern.ch/aggregate-to-cvmfs-csi-nodeplugin: "true"
rules:
  - apiGroups: [""]
    resources: ["configmaps"]
    verbs: ["get", "list"]
  - apiGroups: [""]
    resources: ["nodes"]
    verbs: ["get", "list", "update"]
  - apiGroups: [""]
    resources: ["namespaces"]
    verbs: ["get", "list"]
  - apiGroups: [""]
    resources: ["persistentvolumes"]
    verbs: ["get", "list", "watch", "update"]
  - apiGroups: ["storage.k8s.io"]
    resources: ["volumeattachments"]
    verbs: ["get", "list", "watch", "update"]
---
kind: ClusterRoleBinding
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  name: cvmfs-csi-nodeplugin
subjects:
  - kind: ServiceAccount
    name: cvmfs-csi-nodeplugin
    namespace: kube-system
roleRef:
  kind: ClusterRole
  name: cvmfs-csi-nodeplugin
  apiGroup: rbac.authorization.k8s.io
---
apiVersion: v1
kind: ServiceAccount
metadata:
  name: cvmfs-csi-provisioner
  namespace: kube-system

---
kind: ClusterRole
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  name: cvmfs-external-provisioner-runner
aggregationRule:
  clusterRoleSelectors:
    - matchLabels:
        rbac.cvmfs.csi.cern.ch/aggregate-to-cvmfs-external-provisioner-runner: "true"
rules: []

---
kind: ClusterRole
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  name: cvmfs-external-provisioner-runner-rules
  labels:
    rbac.cvmfs.csi.cern.ch/aggregate-to-cvmfs-external-provisioner-runner: "true"
rules:
  - apiGroups: [""]
    resources: ["nodes"]
    verbs: ["get", "list", "watch"]
  - apiGroups: [""]
    resources: ["secrets"]
    verbs: ["get", "list"]
  - apiGroups: [""]
    resources: ["events"]
    verbs: ["list", "watch", "create", "update", "patch"]
  - apiGroups: [""]
    resources: ["persistentvolumes"]
    verbs: ["get", "list", "watch", "create", "delete"]
  - apiGroups: [""]
    resources: ["persistentvolumeclaims"]
    verbs: ["get", "list", "watch", "update"]
  - apiGroups: ["storage.k8s.io"]
    resources: ["storageclasses"]
    verbs: ["get", "list", "watch"]
  - apiGroups: ["csi.storage.k8s.io"]
    resources: ["csinodeinfos"]
    verbs: ["get", "list", "watch"]
  - apiGroups: ["storage.k8s.io"]
    resources: ["volumeattachments"]
    verbs: ["get", "list", "watch", "update"]

---
kind: ClusterRoleBinding
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  name: cvmfs-csi-provisioner-role
subjects:
  - kind: ServiceAccount
    name: cvmfs-csi-provisioner
    namespace: kube-system
roleRef:
  kind: ClusterRole
  name: cvmfs-external-provisioner-runner
  apiGroup: rbac.authorization.k8s.io

---
kind: Role
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  namespace: kube-system
  name: cvmfs-external-provisioner-cfg
rules:
  - apiGroups: [""]
    resources: ["endpoints"]
    verbs: ["get", "watch", "list", "delete", "update", "create"]
  - apiGroups: [""]
    resources: ["configmaps"]
    verbs: ["get", "list", "create", "delete"]
  - apiGroups: ["coordination.k8s.io"]
    resources: ["leases"]
    verbs: ["get", "watch", "list", "delete", "update", "create"]

---
kind: RoleBinding
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  name: cvmfs-csi-provisioner-role-cfg
  namespace: kube-system
subjects:
  - kind: ServiceAccount
    name: cvmfs-csi-provisioner
    namespace: kube-system
roleRef:
  kind: Role
  name: cvmfs-external-provisioner-cfg
  apiGroup: rbac.authorization.k8s.io
---
apiVersion: storage.k8s.io/v1beta1
kind: CSIDriver
metadata:
  name: csi-cvmfsplugin
  namespace: kube-system
spec:
  attachRequired: false
  podInfoOnMount: false
---
kind: DaemonSet
apiVersion: apps/v1
metadata:
  name: csi-cvmfsplugin
  namespace: kube-system
spec:
  selector:
    matchLabels:
      app: csi-cvmfsplugin
  template:
    metadata:
      labels:
        app: csi-cvmfsplugin
    spec:
      serviceAccount: cvmfs-csi-nodeplugin
      hostNetwork: true
      containers:
        - name: driver-registrar
          image: ${DRIVER_REGISTRAR_IMAGE}
          args:
            - "--v=5"
            - "--csi-address=/csi/csi.sock"
            - "--kubelet-registration-path=/var/lib/kubelet/plugins/cvmfs.csi.cern.ch/csi.sock"
          lifecycle:
            preStop:
              exec:
                command:
                  - "/bin/sh"
                  - "-c"
                  - "rm -rf /registration/csi-cvmfsplugin /registration/csi-cvmfsplugin-reg.sock"
          env:
            - name: KUBE_NODE_NAME
              valueFrom:
                fieldRef:
                  fieldPath: spec.nodeName
          volumeMounts:
            - name: socket-dir
              mountPath: /csi
            - name: registration-dir
              mountPath: /registration
        - name: csi-cvmfsplugin
          securityContext:
            privileged: true
            capabilities:
              add: ["SYS_ADMIN"]
            allowPrivilegeEscalation: true
          image: ${CVMFS_PLUGIN_IMAGE}
          args :
            - "--nodeid=\$(NODE_ID)"
            - "--endpoint=\$(CSI_ENDPOINT)"
            - "--v=5"
            - "--drivername=cvmfs.csi.cern.ch"
          env:
            - name: NODE_ID
              valueFrom:
                fieldRef:
                  fieldPath: spec.nodeName
            - name: POD_NAMESPACE
              valueFrom:
                fieldRef:
                  fieldPath: metadata.namespace
            - name: CSI_ENDPOINT
              value: unix:///csi/csi.sock
          imagePullPolicy: "IfNotPresent"
          volumeMounts:
            - name: mount-cache-dir
              mountPath: /mount-cache-dir
            - name: socket-dir
              mountPath: /csi
            - name: mountpoint-dir
              mountPath: /var/lib/kubelet/pods
              mountPropagation: Bidirectional
            - name: plugin-dir
              mountPath: /var/lib/kubelet/plugins
              mountPropagation: "Bidirectional"
            - name: host-sys
              mountPath: /sys
            - name: lib-modules
              mountPath: /lib/modules
              readOnly: true
            - name: host-dev
              mountPath: /dev
            - name: cvmfs-csi-config
              mountPath: /etc/cvmfs-csi-config/
            - name: keys-tmp-dir
              mountPath: /tmp/csi/keys
            - name: cvmfs-csi-cms-config
              mountPath: /etc/cvmfs/config.d/cms.cern.ch.conf
              subPath: cms.cern.ch.conf
      volumes:
        - name: mount-cache-dir
          emptyDir: {}
        - name: socket-dir
          hostPath:
            path: /var/lib/kubelet/plugins/cvmfs.csi.cern.ch/
            type: DirectoryOrCreate
        - name: registration-dir
          hostPath:
            path: /var/lib/kubelet/plugins_registry/
            type: Directory
        - name: mountpoint-dir
          hostPath:
            path: /var/lib/kubelet/pods
            type: DirectoryOrCreate
        - name: plugin-dir
          hostPath:
            path: /var/lib/kubelet/plugins
            type: Directory
        - name: host-sys
          hostPath:
            path: /sys
        - name: lib-modules
          hostPath:
            path: /lib/modules
        - name: host-dev
          hostPath:
            path: /dev
        - name: cvmfs-csi-config
          configMap:
            name: cvmfs-csi-config
        - name: cvmfs-csi-cms-config
          configMap:
            name: cvmfs-csi-cms-config
            defaultMode: 420
        - name: keys-tmp-dir
          emptyDir: {
            medium: "Memory"
          }
---
kind: Service
apiVersion: v1
metadata:
  name: csi-cvmfsplugin-provisioner
  namespace: kube-system
  labels:
    app: csi-cvmfsplugin-provisioner
spec:
  selector:
    app: csi-cvmfsplugin-provisioner
  ports:
    - name: dummy
      port: 12345

---
kind: Deployment
apiVersion: apps/v1
metadata:
  name: csi-cvmfsplugin-provisioner
  namespace: kube-system
spec:
  selector:
    matchLabels:
      app: csi-cvmfsplugin-provisioner
  replicas: 1
  template:
    metadata:
      labels:
        app: csi-cvmfsplugin-provisioner
    spec:
      serviceAccount: cvmfs-csi-provisioner
      containers:
        - name: csi-provisioner
          image: ${EXTERNAL_PROVISIONER_IMAGE}
          args:
            - "--csi-address=\$(ADDRESS)"
            - "--v=5"
            - "--timeout=60s"
            - "--enable-leader-election=true"
            - "--leader-election-type=leases"
            - "--retry-interval-start=500ms"
          env:
            - name: ADDRESS
              value: unix:///csi/csi-provisioner.sock
          imagePullPolicy: "IfNotPresent"
          volumeMounts:
            - name: socket-dir
              mountPath: /csi
        - name: csi-cvmfsplugin-attacher
          image: ${EXTERNAL_ATTACHER_IMAGE}
          args:
            - "--v=5"
            - "--csi-address=\$(ADDRESS)"
            - "leader-election=true"
            - "--leader-election-type=leases"
          env:
            - name: ADDRESS
              value: unix:///csi/csi-provisioner.sock
          imagePullPolicy: "IfNotPresent"
          volumeMounts:
            - name: socket-dir
              mountPath: /csi
        - name: csi-cvmfsplugin
          securityContext:
            privileged: true
            capabilities:
              add: ["SYS_ADMIN"]
          image: ${CVMFS_PLUGIN_IMAGE}
          args:
            - "--nodeid=\$(NODE_ID)"
            - "--endpoint=\$(CSI_ENDPOINT)"
            - "--v=5"
            - "--drivername=cvmfs.csi.cern.ch"
          env:
            - name: NODE_ID
              valueFrom:
                fieldRef:
                  fieldPath: spec.nodeName
            - name: POD_NAMESPACE
              valueFrom:
                fieldRef:
                  fieldPath: metadata.namespace
            - name: CSI_ENDPOINT
              value: unix:///csi/csi-provisioner.sock
          imagePullPolicy: "IfNotPresent"
          volumeMounts:
            - name: socket-dir
              mountPath: /csi
            - name: host-sys
              mountPath: /sys
            - name: lib-modules
              mountPath: /lib/modules
              readOnly: true
            - name: host-dev
              mountPath: /dev
            - name: cvmfs-csi-config
              mountPath: /etc/cvmfs-csi-config/
            - name: keys-tmp-dir
              mountPath: /tmp/csi/keys
      volumes:
        - name: socket-dir
          hostPath:
            path: /var/lib/kubelet/plugins/cvmfs.csi.cern.ch
            type: DirectoryOrCreate
        - name: host-sys
          hostPath:
            path: /sys
        - name: lib-modules
          hostPath:
            path: /lib/modules
        - name: host-dev
          hostPath:
            path: /dev
        - name: cvmfs-csi-config
          configMap:
            name: cvmfs-csi-config
        - name: keys-tmp-dir
          emptyDir: {
            medium: "Memory"
          }
---
apiVersion: v1
kind: ConfigMap
data:
  config.json: |-
    []
metadata:
  name: cvmfs-csi-config
  namespace: kube-system
---
apiVersion: v1
kind: ConfigMap
metadata:
  name: cvmfs-csi-cms-config
  namespace: kube-system
data:
  cms.cern.ch.conf: |-
    CMS_LOCAL_SITE=T2_CH_CERN
EOF
}

    echo "Waiting for Kubernetes API..."
    until [ "ok" = "$(curl --silent http://127.0.0.1:8080/healthz)" ]
    do
        sleep 5
    done

    kubectl apply --validate=false -f $CVMFS_CSI_DEPLOY
