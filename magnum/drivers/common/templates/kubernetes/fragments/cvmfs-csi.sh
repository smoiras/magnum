#!/bin/bash -x

. /etc/sysconfig/heat-params

step="cvmfs-csi"
printf "Starting to run ${step}\n"

# check the actual version of kubernetes deployed
kube_version=$(curl -k -s https://localhost:6443/version?timeout=32s | sed -rn 's/.*minor": "([[:digit:]]+).*/\1/p')

if [ "$(echo $CVMFS_CSI_ENABLED | tr '[:upper:]' '[:lower:]')" == "true" ]; then
    # deploy CSI 1.x only for 1.14 and above
    if [ "$kube_version" -gt "13" ]; then
        $cvmfs-csi-1x
    else
        $cvmfs-csi-0x
    fi
fi

printf "Finished running ${step}\n"
