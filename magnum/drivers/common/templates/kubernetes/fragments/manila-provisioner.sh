#!/bin/bash -x

. /etc/sysconfig/heat-params

step="manila-provisioner"
printf "Starting to run ${step}\n"

if [ "$(echo $MANILA_ENABLED | tr '[:upper:]' '[:lower:]')" == "true" ]; then

    MANILA_IMAGE="${CONTAINER_INFRA_PREFIX:-docker.io/ceph/}manila-provisioner:${MANILA_VERSION}"

    MANILA_DEPLOY=/srv/magnum/kubernetes/manila-provisioner.yaml

    [ -f ${MANILA_DEPLOY} ] || {
        echo "Writing File: $MANILA_DEPLOY"
        mkdir -p $(dirname ${MANILA_DEPLOY})
        cat << EOF > ${MANILA_DEPLOY}
apiVersion: apps/v1
kind: Deployment
metadata:
  name: manila-provisioner
  namespace: kube-system
  labels:
    app: manila-provisioner
spec:
  replicas: 1
  selector:
    matchLabels:
      app: manila-provisioner
  template:
    metadata:
      labels:
        app: manila-provisioner
    spec:
      serviceAccount: manila-provisioner
      containers:
      - name: manila-provisioner
        image: ${MANILA_IMAGE}
        imagePullPolicy: IfNotPresent
        args: [ "--provisioner=manila-provisioner" ]
---
apiVersion: v1
kind: ServiceAccount
metadata:
  name: manila-provisioner
  namespace: kube-system
---
kind: ClusterRole
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  name: manila-provisioner-runner
  namespace: kube-system
rules:
  - apiGroups: [""]
    resources: ["secrets"]
    verbs: ["create", "get", "delete"]
  - apiGroups: [""]
    resources: ["persistentvolumes"]
    verbs: ["get", "list", "watch", "create", "delete"]
  - apiGroups: [""]
    resources: ["persistentvolumeclaims"]
    verbs: ["get", "list", "watch", "update"]
  - apiGroups: ["storage.k8s.io"]
    resources: ["storageclasses"]
    verbs: ["get", "list", "watch"]
  - apiGroups: [""]
    resources: ["events"]
    verbs: ["list", "watch", "create", "update", "patch"]
---
kind: ClusterRoleBinding
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  name: manila-provisioner-role
  namespace: kube-system
subjects:
  - kind: ServiceAccount
    name: manila-provisioner
    namespace: kube-system
roleRef:
  kind: ClusterRole
  name: manila-provisioner-runner
  apiGroup: rbac.authorization.k8s.io
---
apiVersion: storage.k8s.io/v1beta1
kind: StorageClass
metadata:
  name: geneva-cephfs-testing
provisioner: manila-provisioner
reclaimPolicy: Retain
parameters:
  type: "Geneva CephFS Testing"
  zones: nova
  osSecretName: os-trustee
  osSecretNamespace: kube-system
  protocol: CEPHFS
  backend: csi-cephfs
  csi-driver: cephfs.csi.ceph.com
---
apiVersion: storage.k8s.io/v1beta1
kind: StorageClass
metadata:
  name: meyrin-cephfs
provisioner: manila-provisioner
reclaimPolicy: Retain
parameters:
  type: "Meyrin CephFS"
  zones: nova
  osSecretName: os-trustee
  osSecretNamespace: kube-system
  protocol: CEPHFS
  backend: csi-cephfs
  csi-driver: cephfs.csi.ceph.com
EOF
        }

    echo "Waiting for Kubernetes API..."
    until [ "ok" = "$(curl --silent http://127.0.0.1:8080/healthz)" ]
    do
        sleep 5
    done

    kubectl apply --validate=false -f $MANILA_DEPLOY
fi

printf "Finished running ${step}\n"
