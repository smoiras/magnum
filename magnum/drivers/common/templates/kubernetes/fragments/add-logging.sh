#!/bin/bash -x

. /etc/sysconfig/heat-params

step="add-logging"
printf "Starting to run ${step}\n"

if [ "$(echo ${LOGGING_INSTALLER} | tr '[:upper:]' '[:lower:]')" != "helm" ] && \
   [ ! -z "$LOGGING_PRODUCER" ]; then

    if [ "$(echo "$LOGGING_INCLUDE_INTERNAL" | tr '[:upper:]' '[:lower:]')" != "true" ]; then
        # exclude everything coming from the kube-system pods
        KUBE_SYSTEM_CONFIG_FILTER=$(cat << 'EOF'
    <match kubernetes.var.log.containers.**_kube-system_**>
        @type null
    </match>
EOF
    )
    else
        KUBE_SYSTEM_CONFIG_FILTER=""
    fi

    fluentdConfigMap_file=/srv/magnum/kubernetes/logging/fluentdConfigMap.yaml
    [ -f ${fluentdConfigMap_file} ] || {
        echo "Writing File: $fluentdConfigMap_file"
        mkdir -p "$(dirname ${fluentdConfigMap_file})"
        # NOTE: EOF is escaped to preserve $ and \
        # It has some special variables that are going to be substituted with
        # sed
        CONFIG_MAP_PART_1=$(cat << 'EOF'
apiVersion: v1
kind: ConfigMap
metadata:
  name: fluentd
  namespace: kube-system
data:
  kubernetes.conf: |
    <match fluent.**>
      @type null
    </match>

EOF
    )

        CONFIG_MAP_PART_2=$(cat << 'EOF'

    <source>
      @type tail
      @id in_tail_container_logs
      path /var/log/containers/*.log
      pos_file /var/log/fluentd-containers.log.pos
      tag kubernetes.*
      read_from_head true
      <parse>
        @type json
        time_format %Y-%m-%dT%H:%M:%S.%NZ
      </parse>
    </source>

    <filter kubernetes.**>
      @type kubernetes_metadata
      @id filter_kube_metadata
    </filter>
  fluent.conf: |
EOF
    )

        CONFIG_MAP_DESTINATION=$(cat << EOF
    @include kubernetes.conf

    <filter kubernetes.**>
      @type record_transformer
      enable_ruby
      <record>
        producer "${LOGGING_PRODUCER}"
        type "generic"
        timestamp \${time.to_i * 1000}
        raw \${record["log"].strip}
      </record>
      remove_keys ["log"]
    </filter>

    <match kubernetes.**>
      @type http
      endpoint_url    ${LOGGING_HTTP_DESTINATION}
      serializer      json
      http_method     post
    </match>
EOF
    )

        # we join the two parts and we put the filter in case is necessary
        CONFIG_MAP=$(cat << EOF
$CONFIG_MAP_PART_1
$KUBE_SYSTEM_CONFIG_FILTER
$CONFIG_MAP_PART_2
$CONFIG_MAP_DESTINATION
EOF
    )
        echo "$CONFIG_MAP" > $fluentdConfigMap_file
    }

    echo "Waiting for Kubernetes API..."
    until [ "ok" = "$(curl --silent http://127.0.0.1:8080/healthz)" ]
    do
        sleep 5
    done

    kubectl apply -f $fluentdConfigMap_file


    fluentdDaemonSetRBAC_file=/srv/magnum/kubernetes/logging/fluentdDaemonSetRBAC.yaml
    [ -f ${fluentdDaemonSetRBAC_file} ] || {
        echo "Writing File: $fluentdDaemonSetRBAC_file"
        mkdir -p "$(dirname ${fluentdDaemonSetRBAC_file})"
        # NOTE: EOF needs to be in quotes in order to not escape the $ characters
        cat << EOF > ${fluentdDaemonSetRBAC_file}
---
apiVersion: v1
kind: ServiceAccount
metadata:
  name: fluentd
  namespace: kube-system

---
apiVersion: rbac.authorization.k8s.io/v1beta1
kind: ClusterRole
metadata:
  name: fluentd
  namespace: kube-system
rules:
- apiGroups:
  - ""
  resources:
  - pods
  - namespaces
  verbs:
  - get
  - list
  - watch
---
kind: ClusterRoleBinding
apiVersion: rbac.authorization.k8s.io/v1beta1
metadata:
  name: fluentd
roleRef:
  kind: ClusterRole
  name: fluentd
  apiGroup: rbac.authorization.k8s.io
subjects:
- kind: ServiceAccount
  name: fluentd
  namespace: kube-system
---
apiVersion: apps/v1
kind: DaemonSet
metadata:
  name: fluentd
  namespace: kube-system
  labels:
    k8s-app: fluentd-logging
    version: v1
    kubernetes.io/cluster-service: "true"
spec:
  selector:
    matchLabels:
      k8s-app: fluentd-logging
  template:
    metadata:
      labels:
        k8s-app: fluentd-logging
        version: v1
        kubernetes.io/cluster-service: "true"
    spec:
      serviceAccount: fluentd
      serviceAccountName: fluentd
      tolerations:
      - key: node-role.kubernetes.io/master
        effect: NoSchedule
      - key: dedicated
        value: master
        effect: NoSchedule
      - key: CriticalAddonsOnly
        value: "True"
        effect: NoSchedule
      securityContext:
        seLinuxOptions:
          type: "spc_t"
      containers:
      - name: fluentd
        image: ${CONTAINER_INFRA_PREFIX:-docker.io/fluent/}fluentd-kubernetes-daemonset:${LOGGING_VERSION}-alpine-${LOGGING_TYPE}
        env:
          - name: FLUENTD_CONF
            value: fluent.conf
        resources:
          limits:
            memory: 200Mi
          requests:
            cpu: 100m
            memory: 200Mi
        volumeMounts:
        - name: varlog
          mountPath: /var/log
        - name: varlibdockercontainers
          mountPath: /var/lib/docker/containers
          readOnly: true
        - name: etcfluentd
          mountPath: /fluentd/etc
      terminationGracePeriodSeconds: 30
      volumes:
      - name: varlog
        hostPath:
          path: /var/log
      - name: varlibdockercontainers
        hostPath:
          path: /var/lib/docker/containers
      - name: etcfluentd
        configMap:
          name: fluentd
          items:
            - key: kubernetes.conf
              path: kubernetes.conf
            - key: fluent.conf
              path: fluent.conf
EOF
    }

    echo "Waiting for Kubernetes API..."
    until [ "ok" = "$(curl --silent http://127.0.0.1:8080/healthz)" ]
    do
        sleep 5
    done

    kubectl apply -f $fluentdDaemonSetRBAC_file
fi

printf "Finished running ${step}\n"
