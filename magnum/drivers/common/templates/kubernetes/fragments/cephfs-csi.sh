#!/bin/bash -x

. /etc/sysconfig/heat-params

step="cephfs-csi"
printf "Starting to run ${step}\n"

echo "Waiting for Kubernetes API..."
until [ "ok" = "$(curl --silent http://127.0.0.1:8080/healthz)" ]
do
    sleep 5
done

# check the actual version of kubernetes deployed
kube_version=$(curl -k -s https://localhost:6443/version?timeout=32s | sed -rn 's/.*minor": "([[:digit:]]+).*/\1/p')

if [ "$(echo $CEPHFS_CSI_ENABLED | tr '[:upper:]' '[:lower:]')" == "true" ]; then
    # deploy CSI 1.x only for 1.14 and above
    if [ "$kube_version" -gt "13" ]; then
        $cephfs-csi-1x
    else
        $cephfs-csi-0x
    fi
fi

printf "Finished running ${step}\n"
