    EXTERNAL_PROVISIONER_IMAGE="${CONTAINER_INFRA_PREFIX:-quay.io/k8scsi/}csi-provisioner:${KUBE_CSI_VERSION}"
    DRIVER_REGISTRAR_IMAGE="${CONTAINER_INFRA_PREFIX:-quay.io/k8scsi/}driver-registrar:${KUBE_CSI_VERSION}"
    CVMFS_PLUGIN_IMAGE="${CONTAINER_INFRA_PREFIX:-dockerhub/cvmfs/}cvmfsplugin:${CVMFS_CSI_VERSION}"
    CSI_DEPLOY=/srv/magnum/kubernetes/cvmfs-csi.yaml

    [ -f ${CSI_DEPLOY} ] || {
        echo "Writing File: $CSI_DEPLOY"
        mkdir -p $(dirname ${CSI_DEPLOY})
        cat << EOF > ${CSI_DEPLOY}
kind: Service
apiVersion: v1
metadata:
  namespace: kube-system
  name: csi-cvmfsplugin-provisioner
  labels:
    app: csi-cvmfsplugin-provisioner
spec:
  selector:
    app: csi-cvmfsplugin-provisioner
  ports:
    - name: dummy
      port: 12345

---
kind: StatefulSet
apiVersion: apps/v1beta1
metadata:
  namespace: kube-system
  name: csi-cvmfsplugin-provisioner
spec:
  serviceName: "csi-cvmfsplugin-provisioner"
  replicas: 1
  template:
    metadata:
      labels:
        app: csi-cvmfsplugin-provisioner
    spec:
      serviceAccount: csi-provisioner
      containers:
        - name: csi-provisioner
          image: ${EXTERNAL_PROVISIONER_IMAGE}
          args:
            - "--provisioner=csi-cvmfsplugin"
            - "--csi-address=\$(ADDRESS)"
            - "--v=5"
          env:
            - name: ADDRESS
              value: /var/lib/kubelet/plugins/csi-cvmfsplugin/csi.sock
          imagePullPolicy: "IfNotPresent"
          volumeMounts:
            - name: socket-dir
              mountPath: /var/lib/kubelet/plugins/csi-cvmfsplugin
      volumes:
        - name: socket-dir
          hostPath:
            path: /var/lib/kubelet/plugins/csi-cvmfsplugin
            type: DirectoryOrCreate
---
kind: DaemonSet
apiVersion: apps/v1beta2
metadata:
  namespace: kube-system
  name: csi-cvmfsplugin
spec:
  selector:
    matchLabels:
      app: csi-cvmfsplugin
  template:
    metadata:
      labels:
        app: csi-cvmfsplugin
    spec:
      serviceAccount: csi-nodeplugin
      hostNetwork: true
      # to use e.g. Rook orchestrated cluster, and mons' FQDN is
      # resolved through k8s service, set dns policy to cluster first
      dnsPolicy: ClusterFirstWithHostNet      
      containers:
        - name: driver-registrar
          image: ${DRIVER_REGISTRAR_IMAGE}
          args:
            - "--v=5"
            - "--csi-address=\$(ADDRESS)"
            - "--mode=pluginwatcher-register"
            - "--kubelet-registration-path=\$(DRIVER_REG_SOCK_PATH)"
            - "--driver-requires-attachment=false"
          env:
            - name: ADDRESS
              value: /var/lib/kubelet/plugins/csi-cvmfsplugin/csi.sock
            - name: DRIVER_REG_SOCK_PATH
              value: /var/lib/kubelet/plugins/csi-cvmfsplugin/csi.sock
            - name: KUBE_NODE_NAME
              valueFrom:
                fieldRef:
                  fieldPath: spec.nodeName
          volumeMounts:
            - name: socket-dir
              mountPath: /var/lib/kubelet/plugins/csi-cvmfsplugin
            - name: registration-dir
              mountPath: /registration
        - name: csi-cvmfsplugin
          securityContext:
            privileged: true
            capabilities:
              add: ["SYS_ADMIN"]
            allowPrivilegeEscalation: true
          image: ${CVMFS_PLUGIN_IMAGE}
          args :
            - "--nodeid=\$(NODE_ID)"
            - "--endpoint=\$(CSI_ENDPOINT)"
            - "--v=5"
            - "--drivername=csi-cvmfsplugin"
          env:
            - name: NODE_ID
              valueFrom:
                fieldRef:
                  fieldPath: spec.nodeName
            - name: CSI_ENDPOINT
              value: unix://var/lib/kubelet/plugins/csi-cvmfsplugin/csi.sock
          imagePullPolicy: "IfNotPresent"
          volumeMounts:
            - name: plugin-dir
              mountPath: /var/lib/kubelet/plugins/csi-cvmfsplugin
            - name: pods-mount-dir
              mountPath: /var/lib/kubelet/pods
              mountPropagation: "Bidirectional"
            - mountPath: /sys
              name: host-sys
            - name: lib-modules
              mountPath: /lib/modules
              readOnly: true
            - name: host-dev
              mountPath: /dev
      volumes:
        - name: plugin-dir
          hostPath:
            path: /var/lib/kubelet/plugins/csi-cvmfsplugin
            type: DirectoryOrCreate
        - name: registration-dir
          hostPath:
            path: /var/lib/kubelet/plugins/
            type: Directory
        - name: pods-mount-dir
          hostPath:
            path: /var/lib/kubelet/pods
            type: Directory
        - name: socket-dir
          hostPath:
            path: /var/lib/kubelet/plugins/csi-cvmfsplugin
            type: DirectoryOrCreate
        - name: host-sys
          hostPath:
            path: /sys
        - name: lib-modules
          hostPath:
            path: /lib/modules
        - name: host-dev
          hostPath:
            path: /dev
EOF
        }

    echo "Waiting for Kubernetes API..."
    until [ "ok" = "$(curl --silent http://127.0.0.1:8080/healthz)" ]
    do
        sleep 5
    done

    kubectl apply --validate=false -f $CSI_DEPLOY
